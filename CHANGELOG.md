# Selphybot Changelog
Updates are listed in reverse chronological order.

### 1.1 (dev)
- cooldowns are now stored per-user, not globally, and no longer apply in DMs
- multiple admins can now be defined in the config
- admins can now use DM-only commands anywhere

## 1.0
Marks the first release after the initial refactoring. This contains:
- the entire logic registerCommand
    - various matching modes for commands
    - auto moderation actions
    - different switches to control command behavior
    - custom functions as parameters to more advanced commands
    - cooldowns for each command
- handling of newly joined users
- the ability to add an admin
    - and special commands for them
    - admins can use commands that are on cooldown
- lots of specific functions used on Selphy’s server
- probably more that I’m forgetting
