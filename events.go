package main

import (
    "fmt"
    "github.com/bwmarrin/discordgo"
    "log"
)

func onJoin(s *discordgo.Session, member *discordgo.GuildMemberAdd) {
    if !member.User.Bot && config.RequireAccept {
        s.GuildMemberRoleAdd(config.ServerID, member.User.ID, config.LockedRoleID)
    }
    if !member.User.Bot && config.SendWelcomeDM {
        dm, err := s.UserChannelCreate(member.User.ID)
        if err != nil {
            log.Println(fmt.Sprintf("Error creating DM with %s", userToString(member.User), err))
        } else {
            embed := getWelcomeEmbed()
            _, err = s.ChannelMessageSendEmbed(dm.ID, embed)
            if err != nil {
                log.Println(fmt.Sprintf("Error sending DM to %s", userToString(member.User), err))
            }
        }
        if err != nil {
            // if any of the preceding operations produced an error
            log.Printf("Sending welcome @mention at %s", userToString(member.User))
            s.ChannelMessageSend(config.GeneralChannel, fmt.Sprintf("Wilkommen <@%s>. Bitte aktiviere vorübergehend DMs für diesen Server und sende eine Nachricht mit !welcome an mich.", member.User.ID))
        }
    }
    log.Printf("User joined: %s", userToString(member.User))
}

func onDM(s *discordgo.Session, m *discordgo.MessageCreate) {
    log.Printf("Received DM from %s with content: “%s”", userToString(m.Author), m.Content)
    fmt.Sprintf("Received DM from %s with content: “%s”", userToString(m.Author), m.Content)
    Member, _ := s.GuildMember(config.ServerID, m.Author.ID)
    dm, _ := s.UserChannelCreate(Member.User.ID)
    for comm, role := range config.RoleCommands {
        if m.Content == comm {
            for _, irole := range config.RoleCommands {
                for _, mrole := range Member.Roles {
                    if irole == mrole {
                        s.ChannelMessageSend(dm.ID, "Baka, du kannst nur eine der Rollen haben.")
                        log.Printf("Denied Role %s to %s. User already has %s", roleName(s.State, irole), userToString(m.Author), roleName(s.State, irole))
                        return
                    }
                }
            }
            log.Printf("Giving Role %s to %s", roleName(s.State, role), userToString(m.Author))
            s.ChannelMessageSend(dm.ID, "Haaai, Ryoukai desu~")
            s.GuildMemberRoleAdd(config.ServerID, m.Author.ID, role)
        }
    }
}

