package main

import (
    "fmt"
    "os/signal"
    "os"
    "syscall"
    "log"
    "github.com/bwmarrin/discordgo"
)

var config = readConfig()
var commands []Command

func main() {
    dg, err := discordgo.New("Bot " + config.Token)
    if err != nil {
        fmt.Println("error: ", err)
        return
    }

    dg.AddHandler(evaluateMessage)
    dg.AddHandler(onJoin)
    err = dg.Open()
    if err != nil {
        fmt.Println("No connection:\n", err)
        return
    }

    f, err := os.OpenFile("selphybot.log", os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
    if err != nil {
        fmt.Println("Error opening log file:\n", err)
    }
    defer f.Close()
    log.SetOutput(f)
    addCommands()

    fmt.Println("Bot running. selphyWoo")
    log.Println("Bot running. selphyWoo")
    sc := make(chan os.Signal, 1)
    signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
    <-sc

    fmt.Println("Exiting...")
    log.Println("Exiting...")

    dg.Close()
}

// I’ll just put all of the commands here for now.
func addCommands() {
    // Moderation
    registerCommand(Command{Trigger: "^[^`]*([()|DoO];|;[()|DoOpP]|:wink:|😉)[^`]*$", Output: "<@%s> Oboe!", DeleteInput: true, OutputIsReply: true, Type: CommandTypeRegex})
    registerCommand(Command{Trigger: "!complain", Type: CommandTypePrefix, DMOnly: true, Function: redirectComplaint})
    registerCommand(Command{Trigger: "!beschwerde", Type: CommandTypePrefix, DMOnly: true, Function: redirectComplaint})

    for comm, _ := range config.RoleCommands {
        registerCommand(Command{Trigger: comm, Type: CommandTypeFullMatch, DMOnly: true, Function: giveAgeRole})
    }

    // Misc commands
    registerCommand(Command{Trigger: "o/", Output: "\\o", Type: CommandTypeFullMatch, Cooldown: 10})
    registerCommand(Command{Trigger: "\\o", Output: "o/", Type: CommandTypeFullMatch, Cooldown: 10})
    registerCommand(Command{Trigger: "\\o/", Output: "/o\\", Type: CommandTypeFullMatch, Cooldown: 10})
    registerCommand(Command{Trigger: "/o\\", Output: "\\o/", Type: CommandTypeFullMatch, Cooldown: 10})
    registerCommand(Command{Trigger: "<:selphyDango:441001954542616576>", Output: "<:dango:430669469799677953> :notes: だんごだんごだんごだんごだんご大家族 :notes: <:dango:430669469799677953>", Type: CommandTypeFullMatch, Cooldown: 1200})
    registerCommand(Command{Trigger: "praise the sun", Output: "If only I could be so grossly incandescent \\\\[T]/", Type: CommandTypeContains, IgnoreCase: true, Cooldown: 85600})

    // Information
    registerCommand(Command{Trigger: "!welcome", OutputEmbed: getWelcomeEmbed(), Type: CommandTypeFullMatch, DMOnly: true})

    // Admin and/or debug
    registerCommand(Command{Trigger: "<@%s> <3", Output: "<@%s> <3", Type: CommandTypeFullMatch, AdminOnly: true, OutputIsReply: true, RequiresMention: true})
    registerCommand(Command{Trigger: "echo", Type: CommandTypePrefix, Function: echoMessage, AdminOnly: true})

    fmt.Printf("Successfully initialized %d commands\n", len(commands))
    log.Printf("Successfully initialized %d commands", len(commands))
}

